package com.astrosine.app;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;

public class ApplicationClass extends Application {

    private static Context context;
    public static final String CHANNEL_1_ID = "channel_1";

    public ApplicationClass() {
        context=this;
    }

    public ApplicationClass(Context context) {
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannels();
    }

    public static Context getContext() {
        return context;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel1 = new NotificationChannel(
                    CHANNEL_1_ID,
                    "channel_1",
                    NotificationManager.IMPORTANCE_HIGH
            );

            channel1.enableLights(true);
            channel1.enableVibration(true);
            channel1.setImportance(NotificationManager.IMPORTANCE_HIGH);
            /* channel1.setBypassDnd(true);*/
            channel1.setLightColor(Color.argb(0,128,0,128));
            // channel1.setVibrationPattern(new long[] { 1000, 1000, 1000, 1000, 1000 });
            channel1.setDescription("this is channel 1");




            NotificationManager manager = getSystemService(NotificationManager.class);
            //  manager.createNotificationChannel(defaultchannel);
            manager.createNotificationChannel(channel1);


          /*  NotificationManager default1= (NotificationManager) getContext().getSystemService(NOTIFICATION_SERVICE);
            default1.createNotificationChannel(channel1);*/

        }
    }
}
