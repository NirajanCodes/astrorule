package com.astrosine.app.Networking;

public interface NetworkResponse {

     void onSuccess(String response);

     void onError(String response);


}
