package com.astrosine.app.Networking;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.StringRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.astrosine.app.Utility.SharedPreferencesManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class Networking {

    public static void getAboutUsData(NetworkResponse networkResponse, Context context, String url) {
        getRequest(networkResponse, context, url);
    }

    public static void getRequest(final NetworkResponse networkResponse, Context context, String url) {
        AndroidNetworking.get(url)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        networkResponse.onSuccess(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        networkResponse.onError(error.getErrorBody());
                    }
                });
    }

    public static void postRequest(final NetworkResponse networkResponse, JSONObject jsonObject, String url) {
        AndroidNetworking.post(url)
                .addJSONObjectBody(jsonObject)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        networkResponse.onSuccess(response.toString());
                    }

                    @Override
                    public void onError(ANError error) {
                        networkResponse.onError(error.getErrorBody());

                    }
                });
    }

    public static void postMultipartRequest(final NetworkResponse networkResponse, String multipartFileBody, String url, File file) {
        String name = null, country = null, accuracy = null, gender = null, email = null, city = null, dob = null, tob = null;
        try {
            JSONObject jsonObject = new JSONObject(multipartFileBody);
            name = jsonObject.getString("name");
            country = jsonObject.getString("country");
            accuracy = jsonObject.getString("time_accuracy");
            gender = jsonObject.getString("gender");
            email = jsonObject.getString("email");
            city = jsonObject.getString("location");
            dob = jsonObject.getString("date_of_birth");
            tob = jsonObject.getString("time_of_birth");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (file == null) {

            AndroidNetworking.upload(url)
                    .addMultipartParameter("name", name)
                    .addMultipartParameter("country", country)
                    .addMultipartParameter("device_token", SharedPreferencesManager.getDeviceId())
                    .addMultipartParameter("time_accuracy", String.valueOf(accuracy))
                    .addMultipartParameter("gender", gender)
                    .addMultipartParameter("email", email)
                    .addMultipartParameter("device_type", "android")
                    .addMultipartParameter("location", city)
                    .addMultipartParameter("gcm_id", SharedPreferencesManager.getGcmId())
                    .addMultipartParameter("date_of_birth", dob)
                    .addMultipartParameter("time_of_birth", tob)
                    .setTag("uploadImage")
                    .setPriority(Priority.HIGH)
                    .build()
                    .setUploadProgressListener(new UploadProgressListener() {
                        @Override
                        public void onProgress(long bytesUploaded, long totalBytes) {
                            // do anything with progress
                        }
                    })
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String response) {
                            networkResponse.onSuccess(response);

                        }

                        @Override
                        public void onError(ANError anError) {
                            networkResponse.onError(anError.toString());

                        }
                    });
        } else {
            AndroidNetworking.upload(url)
                    .addMultipartFile("image", file)
                    .addMultipartParameter("name", name)
                    .addMultipartParameter("country", country)
                    .addMultipartParameter("device_token", SharedPreferencesManager.getDeviceId())
                    .addMultipartParameter("time_accuracy", String.valueOf(accuracy))
                    .addMultipartParameter("gender", gender)
                    .addMultipartParameter("email", email)
                    .addMultipartParameter("device_type", "android")
                    .addMultipartParameter("location", city)
                    .addMultipartParameter("gcm_id", SharedPreferencesManager.getGcmId())
                    .addMultipartParameter("date_of_birth", dob)
                    .addMultipartParameter("time_of_birth", tob)
                    .setTag("uploadImage")
                    .setPriority(Priority.HIGH)
                    .build()
                    .setUploadProgressListener(new UploadProgressListener() {
                        @Override
                        public void onProgress(long bytesUploaded, long totalBytes) {
                            // do anything with progress
                        }
                    })
                    .getAsString(new StringRequestListener() {
                        @Override
                        public void onResponse(String response) {
                            networkResponse.onSuccess(response);

                        }

                        @Override
                        public void onError(ANError anError) {
                            networkResponse.onError(anError.toString());

                        }
                    });
        }

    }
}