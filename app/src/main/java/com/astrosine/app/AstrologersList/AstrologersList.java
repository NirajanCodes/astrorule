package com.astrosine.app.AstrologersList;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.astrosine.app.Constants.Constants;
import com.astrosine.app.Networking.NetworkResponse;
import com.astrosine.app.Networking.Networking;
import com.astrosine.app.Notification.Notification;
import com.astrosine.app.R;
import com.astrosine.app.Utility.Utility;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AstrologersList extends Fragment {
    private String choosenAstrologer;
    private AstrologersViewAdapter.chooseAstrologer chooseAstrologer;

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Resources.Theme theme = getActivity().getTheme();
        Utility.hideKeyboard(getActivity());
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ImageView backbutton = toolbar.findViewById(R.id.navigation);
        backbutton.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        TextView title = toolbar.findViewById(R.id.title);
        title.setText("ASTROLOGERS LIST");
        final AVLoadingIndicatorView progressBar = toolbar.findViewById(R.id.indicator);
        final ImageView notification = toolbar.findViewById(R.id.notification);
        notification.setVisibility(View.GONE);
        final FragmentManager framentManager = getActivity().getSupportFragmentManager();
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Notification notification = new Notification();
                framentManager.beginTransaction().replace(R.id.content_main,notification).addToBackStack("Notification").commit();
            }
        });
        final ArrayList<AstrologersListModel> arrayListModel = new ArrayList();
        NetworkResponse networkResponse = new NetworkResponse() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject status = jsonObject.getJSONObject("status");
                    int responseCode = status.getInt("responseCode");
                    JSONArray body = jsonObject.getJSONArray("body");
                    if (responseCode == 200) {
                        for (int i = 0; i < body.length(); i++) {
                            JSONObject data = body.getJSONObject(i);
                            AstrologersListModel astrologersListModel = new AstrologersListModel();
                            astrologersListModel.setAbout(data.getString("about"));
                            astrologersListModel.setImageUrl(data.getString("image"));
                            astrologersListModel.setName(data.getString("name"));
                            arrayListModel.add(astrologersListModel);
                        }
                    }
                    RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
                    AstrologersViewAdapter astrologersAdapter = new AstrologersViewAdapter(arrayListModel, chooseAstrologer);
                    recyclerView.setAdapter(astrologersAdapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
                notification.setVisibility(View.VISIBLE);
            }
            @Override
            public void onError(String response) {
            }
        };
        Networking.getRequest(networkResponse, getContext(), Constants.ASTROLOGERSLIST);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return LayoutInflater.from(container.getContext()).inflate(R.layout.fragment_astrologers_list, container, false);
    }
}
