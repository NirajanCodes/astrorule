package com.astrosine.app.AstrologersList;

public class AstrologersListModel {

    private String imageUrl;
    private String name;
    private String about;
    private String id;

    public String getId() {
        return id;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getName() {
        return name;
    }

    public String getAbout() {
        return about;
    }
}
