package com.astrosine.app.AstrologersList;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.astrosine.app.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AstrologersViewAdapter extends RecyclerView.Adapter<AstrologersViewAdapter.ViewHolder> {
    private ArrayList<AstrologersListModel> arrayListModel;
    private AstrologersViewAdapter.chooseAstrologer chooseAstrologer;

    public AstrologersViewAdapter(ArrayList<AstrologersListModel> arrayListModel, AstrologersViewAdapter.chooseAstrologer chooseAstrologer) {
        this.arrayListModel = arrayListModel;
        this.chooseAstrologer = chooseAstrologer;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyler_astrologers_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {

        viewHolder.about.setText(Html.fromHtml(arrayListModel.get(i).getAbout()));
        viewHolder.name.setText(Html.fromHtml(arrayListModel.get(i).getName()));
        if (chooseAstrologer != null) {
            viewHolder.astologer_box.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    chooseAstrologer.chooseAstrologer(arrayListModel.get(viewHolder.getAdapterPosition()).getId());
                }
            });
        }
        Picasso.get().load(arrayListModel.get(i).getImageUrl()).resize(100, 100).into(viewHolder.image);
    }

    @Override
    public int getItemCount() {
        return arrayListModel.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name, about;
        ImageView image;
        CardView astologer_box;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            image = itemView.findViewById(R.id.iv_image);
            about = itemView.findViewById(R.id.desc);
            astologer_box = itemView.findViewById(R.id.astrologers_box);
        }
    }

    public interface chooseAstrologer {
        void chooseAstrologer(String id);
    }
}
