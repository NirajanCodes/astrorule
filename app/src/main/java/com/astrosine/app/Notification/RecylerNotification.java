package com.astrosine.app.Notification;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.astrosine.app.R;
import com.astrosine.app.Utility.SharedPreferencesManager;

import org.json.JSONArray;

import java.util.ArrayList;


public class RecylerNotification extends RecyclerView.Adapter<RecylerNotification.ViewHolder> {

    private ArrayList<ModelNotification> arrayList;
    private ClickForDetail clickForDetail;
    private JSONArray data;
    private Context context;

    RecylerNotification(ArrayList<ModelNotification> arrayList, ClickForDetail clickForDetail) {
        this.arrayList = arrayList;
        this.clickForDetail = clickForDetail;
        try {
            data = new JSONArray(SharedPreferencesManager.getSeentId());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_view_holder, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.notice.setText(Html.fromHtml(arrayList.get(i).getNotice()));
        viewHolder.notice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickForDetail.clickForDetail(arrayList.get(i).getNotice(), arrayList.get(i).getId());
            }
        });
        try {
            for (int k = 0; k < data.length(); k++) {
                if (data.get(k).toString().equalsIgnoreCase(arrayList.get(i).getId())) {
                    viewHolder.notice.setTextColor(context.getResources().getColor(R.color.colorView2));
                    viewHolder.date.setTextColor(context.getResources().getColor(R.color.colorView2));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] date = arrayList.get(i).getDate().split(" ");
        viewHolder.date.setText(date[0] + "   " + date[1] );
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView notice, date;
        ImageView done;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            notice = itemView.findViewById(R.id.tv_info);
            date = itemView.findViewById(R.id.date);
            done = itemView.findViewById(R.id.done);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }

    interface ClickForDetail {
        void clickForDetail(String text, String id);
    }
}
