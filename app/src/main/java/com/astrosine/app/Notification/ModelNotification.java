package com.astrosine.app.Notification;

public class ModelNotification {
    private String id;
    private String notice;
    private String isRead;
    private String date;

    public void setId(String id) {
        this.id = id;
    }

    public void setNotice(String notice) {
        this.notice = notice;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getId() {
        return id;
    }

    public String getNotice() {
        return notice;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }
}
