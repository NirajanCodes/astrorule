package com.astrosine.app.Notification;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.astrosine.app.R;
import com.wang.avi.AVLoadingIndicatorView;

public class Details extends Fragment {
    private TextView text;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        text = view.findViewById(R.id.details);
        text.setText(getArguments().getString("text"));
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        TextView title = toolbar.findViewById(R.id.title);
        title.setText("NOTIFICATIONS");
        ImageView backbutton = toolbar.findViewById(R.id.navigation);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        backbutton.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        ImageView notification = toolbar.findViewById(R.id.notification);
        notification.setVisibility(View.GONE);
        final AVLoadingIndicatorView progress = toolbar.findViewById(R.id.indicator);
        progress.setVisibility(View.GONE);


    }
}
