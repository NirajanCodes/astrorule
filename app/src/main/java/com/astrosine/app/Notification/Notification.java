package com.astrosine.app.Notification;

import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.astrosine.app.Constants.Constants;
import com.astrosine.app.MainActivity;
import com.astrosine.app.Networking.NetworkResponse;
import com.astrosine.app.Networking.Networking;
import com.astrosine.app.R;
import com.astrosine.app.Utility.SharedPreferencesManager;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Notification extends Fragment {
    private JSONObject jsonObject;
    private RecyclerView recyclerView;
    JSONArray data = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.notification, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        TextView title = toolbar.findViewById(R.id.title);
        title.setText("NOTIFICATIONS");
        recyclerView = view.findViewById(R.id.recycler_view);
        if (SharedPreferencesManager.getSeentId() != null) {
            try {
                data = new JSONArray(SharedPreferencesManager.getSeentId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ImageView backbutton = toolbar.findViewById(R.id.navigation);
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        backbutton.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        ImageView notification = toolbar.findViewById(R.id.notification);
        notification.setVisibility(View.GONE);
        final AVLoadingIndicatorView progress = toolbar.findViewById(R.id.indicator);
        NetworkResponse networkResponse = new NetworkResponse() {
            @Override
            public void onSuccess(String response) {
                try {

                    ArrayList<ModelNotification> myList = new ArrayList<>();
                    Log.d("nirajan", "notification " + response);
                    progress.setVisibility(View.GONE);
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("body");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        ModelNotification modelNotification = new ModelNotification();
                        modelNotification.setNotice(jsonObject1.getString("notice"));
                        modelNotification.setDate(jsonObject1.getString("created_at"));
                        modelNotification.setId(jsonObject1.getString("id"));
                        myList.add(modelNotification);
                        myList.size();
                    }
                    RecylerNotification recylerNotification = new RecylerNotification(myList, new RecylerNotification.ClickForDetail() {
                        @Override
                        public void clickForDetail(String text, String id) {
                            final FragmentManager framentManager = getActivity().getSupportFragmentManager();
                            Details notification = new Details();
                            Bundle bundle = new Bundle();
                            bundle.putString("text", text);
                            notification.setArguments(bundle);
                            try {

                                if (SharedPreferencesManager.getSeentId() != null) {
                                    try {
                                        data = new JSONArray(SharedPreferencesManager.getSeentId());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    data = new JSONArray();
                                }

                                    data.put(id);


                            }catch (Exception e) {
                                e.printStackTrace();
                            }
                            SharedPreferencesManager.setSeenId(data.toString());
                            framentManager.beginTransaction().replace(R.id.content_main, notification).addToBackStack("Notification").commit();
                        }
                    });
                    recyclerView.setAdapter(recylerNotification);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String response) {
                Log.d("nirajan", "notification " + response);

            }
        };
        Networking.getRequest(networkResponse, getContext(), Constants.NOTIFICATION);


    }

}
