package com.astrosine.app.Constants;

public interface Constants {

    String BASEURL = "https://astrorule.com/apis/public/api/";
    String ABOUTUS = BASEURL + "aboutus";
    String ASTROLOGERSLIST = BASEURL + "astrologer";
    String PROFILE = BASEURL + "profile";
    String HELP = BASEURL + "help";
    String COUNTRY = BASEURL + "countries";
    String SPLASH = BASEURL + "splash";
    String QUESTIONS = BASEURL + "question";
    String NOTIFICATION = BASEURL + "special-notice";
    String CHAT = BASEURL + "chat/";
    String PAYMENTFAILURE = BASEURL + "payment-failed-question";
    String DELETECHAT = BASEURL+"chat/delete-message";
    String RATE = BASEURL+"rate/";
//    String STATES=BASEURL+"Nepal/states";
}
