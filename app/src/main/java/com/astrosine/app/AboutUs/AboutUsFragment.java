package com.astrosine.app.AboutUs;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;


import com.astrosine.app.Constants.Constants;
import com.astrosine.app.Networking.NetworkResponse;
import com.astrosine.app.Networking.Networking;
import com.astrosine.app.Notification.Notification;
import com.astrosine.app.R;
import com.astrosine.app.Utility.Utility;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;
import com.wang.avi.Indicator;

import org.json.JSONException;
import org.json.JSONObject;

public class AboutUsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragmen_about_us, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final TextView aboutText = view.findViewById(R.id.tv_text);
        final ImageView aboutImage = view.findViewById(R.id.iv_image);
        final Resources.Theme theme = getActivity().getTheme();
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        final AVLoadingIndicatorView progressBar = toolbar.findViewById(R.id.indicator);
        ImageView backbutton = toolbar.findViewById(R.id.navigation);
        final ImageView notification = toolbar.findViewById(R.id.notification);
        notification.setVisibility(View.GONE);
        final FragmentManager  framentManager = getActivity().getSupportFragmentManager();
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Notification notification = new Notification();
                framentManager.beginTransaction().replace(R.id.content_main, notification).addToBackStack("Notification").commit();
            }
        });

        backbutton.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        TextView title = toolbar.findViewById(R.id.title);
        title.setText("ABOUT US");
        NetworkResponse networkResponse = new NetworkResponse() {
            @Override
            public void onSuccess(String response) {
                Log.d(Utility.TAG, "response : " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject body = jsonObject.getJSONObject("body");
                    String image = body.getString("image");
                    String data = body.getString("data");
                    aboutText.setText(Html.fromHtml(data));
                    Picasso.get().load(image).into(aboutImage);
                    progressBar.setVisibility(View.GONE);
                    notification.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String response) {

            }
        };
        Networking.getAboutUsData(networkResponse, getContext(), Constants.ABOUTUS);


        //getActivity().setActionBar();

    }
}
