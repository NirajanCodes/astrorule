package com.astrosine.app.Utility;

public  class NotificationEvent {
    private int data = 0;

    public NotificationEvent(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
