package com.astrosine.app.Utility;

import android.content.Context;
import android.content.SharedPreferences;

import com.astrosine.app.ApplicationClass;

import org.json.JSONObject;

public class SharedPreferencesManager {

    private static final String SHAREDPREFNAME = "MyPrefs";

    public SharedPreferencesManager() {

    }

    private static SharedPreferences getSharedPreferences() {

        return ApplicationClass.getContext().getSharedPreferences(SHAREDPREFNAME, Context.MODE_PRIVATE);

    }

    public static Boolean getFirstTime() {
        return getSharedPreferences().getBoolean("walkThrough", false);

    }

    public static void setFirstTime(Boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean("walkThrough", value);
        editor.apply();
    }

    public static boolean getRegistered() {
        return getSharedPreferences().getBoolean("registered", false);

    }

    public static void setRegistered(boolean value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean("registered", value);
        editor.apply();
    }

    public static String getUserData() {
        return getSharedPreferences().getString("userData", null);
    }

    public static void setUserData(String value) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("userData", value);
        editor.apply();
    }

    public static void setUserName(String name) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("userName", name);
        editor.apply();
    }

    public static String getUserName() {
        return getSharedPreferences().getString("userName", null);
    }

    public static void setUserDob(String dob) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("dob", dob);
        editor.apply();
    }

    public static String getUserDob() {
        return getSharedPreferences().getString("dob", null);
    }

    public static void setDeviceId(String android_id) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("deviceId", android_id);
        editor.apply();
    }

    public static String getDeviceId() {
        return getSharedPreferences().getString("deviceId", null);
    }

    public static void setImageUrl(String image_url) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("imageUrl", image_url);
        editor.apply();
    }

    public static String getImageUrl() {
        return getSharedPreferences().getString("imageUrl", null);
    }

    public static void setUserId(String user_id) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("userId", user_id);
        editor.apply();
    }

    public static String getUserId() {
        return getSharedPreferences().getString("userId", null);
    }

    public static void setGcmId(String token) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("gcmId", token);
        editor.apply();
    }

    public static String getGcmId() {
        return getSharedPreferences().getString("gcmId", null);
    }

    public static void setAstrologerId(String id) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("astrologersId", id);
        editor.apply();
    }

    public static String getAstrologersId() {
        return getSharedPreferences().getString("astrologersId", null);
    }

    public static void setDefaultId(boolean b) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putBoolean("defaultId", b);
        editor.apply();

    }

    public static boolean getDefaultId() {
        return getSharedPreferences().getBoolean("defaultId", false);

    }

    public static void setSeenId(String seen) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("seenId", seen);
        editor.apply();

    }

    public static String getSeentId() {
        return getSharedPreferences().getString("seenId", null);
    }

    public static String getPrice() {
        return getSharedPreferences().getString("price", null);
    }

    public static void setPrice(String skuprice) {
        SharedPreferences.Editor editor = getSharedPreferences().edit();
        editor.putString("price", skuprice);
        editor.apply();
    }
}
