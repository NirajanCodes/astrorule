package com.astrosine.app.SQLiteDatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.ListView;

import com.astrosine.app.Chat.ConverstionPOJO;

import java.util.ArrayList;

public class AstroDatabase extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "astrosine_database";
    private static final int DATABSE_VERSION = 1;
    private static final String CHATTABLE = "chat";
    private static final String ID = "Id";
    private static final String MESSAGE = "Message";
    private static final String FLAG = "flag";

    public AstroDatabase(Context context) {
        super(context,  DATABASE_NAME, null, DATABSE_VERSION);
    }

    private static final String CREATE_CHAT_TABLE = " CREATE TABLE " + CHATTABLE + " ( " +
            ID + " int , " +
            MESSAGE + " varchar , " +
            FLAG + " varchar " +
            " ) ";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CHAT_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CHATTABLE);
        db.execSQL(CREATE_CHAT_TABLE);

    }

    public void insertChatTable(int id,String message, String flag) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(MESSAGE, message);
        contentValues.put(FLAG, flag);
        contentValues.put(ID, id);
        sqLiteDatabase.insertOrThrow(CHATTABLE, null, contentValues);
    }

    public ArrayList<ConverstionPOJO> getChatData() {

        String selectQuery = "SELECT * FROM " + CHATTABLE;
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);
        ArrayList<ConverstionPOJO> arrayList = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                String message = cursor.getString(cursor.getColumnIndexOrThrow(MESSAGE));
                String flag = cursor.getString(cursor.getColumnIndexOrThrow(FLAG));
                int  id =cursor.getInt(cursor.getColumnIndexOrThrow(ID));
                boolean realFlag;
                if(flag.equalsIgnoreCase("true")){
                    realFlag=true;
                }else{
                    realFlag=false;
                }
                //ConverstionPOJO converstionPOJO = new ConverstionPOJO(message, realFlag, id);
            //    arrayList.add(converstionPOJO);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }
}
