package com.astrosine.app.Chat;

public class ConverstionPOJO {
    private String messages;
    private boolean isQuestion;
    private boolean paymentFailed;
    private int id;
    private int rating;
    private int answerId;
    private String moderatorName;
    private String moderatorImage;
    private String astrologerImage;
    private String astrologerName;
    private String type;
    private boolean flag;

    public void setFlag(boolean flag) {

        this.flag = flag;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public ConverstionPOJO(String messages, boolean isQuestion, boolean paymentFailed, int id,String type,boolean flag, int rating, int answerId) {
        this.messages = messages;
        this.isQuestion = isQuestion;
        this.id = id;
        this.moderatorName = moderatorName;
        this.moderatorImage = moderatorImage;
        this.astrologerImage = astrologerImage;
        this.astrologerName = astrologerName;
        this.paymentFailed=paymentFailed;
        this.type=type;
        this.flag=flag;
        this.rating = rating;
        this.answerId = answerId;
    }

    public int getAnswerId() {
        return answerId;
    }

    public void setAnswerId(int answerId) {
        this.answerId = answerId;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getAstrologerImage() {
        return astrologerImage;
    }

    public void setAstrologerImage(String astrologerImage) {
        this.astrologerImage = astrologerImage;
    }

    public String getAstrologerName() {
        return astrologerName;
    }

    public void setAstrologerName(String astrologerName) {
        this.astrologerName = astrologerName;
    }

    public String getMessages() {
        return messages;
    }

    public void setMessages(String messages) {
        this.messages = messages;
    }

    public void setPaymentFailed(boolean paymentFailed) {
        this.paymentFailed = paymentFailed;
    }

    public boolean isPaymentFailed() {
        return paymentFailed;
    }

    public boolean isQuestion() {
        return isQuestion;
    }

    public void setQuestion(boolean question) {
        isQuestion = question;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModeratorName() {
        return moderatorName;
    }

    public void setModeratorName(String moderatorName) {
        this.moderatorName = moderatorName;
    }

    public String getModeratorImage() {
        return moderatorImage;
    }

    public void setModeratorImage(String moderatorImage) {
        this.moderatorImage = moderatorImage;
    }
}