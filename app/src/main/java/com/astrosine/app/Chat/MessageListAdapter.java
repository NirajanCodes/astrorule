package com.astrosine.app.Chat;

import android.app.Activity;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.astrosine.app.Constants.Constants;
import com.astrosine.app.Networking.NetworkResponse;
import com.astrosine.app.Networking.Networking;
import com.astrosine.app.R;
import com.astrosine.app.Utility.SharedPreferencesManager;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class MessageListAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;
    private Activity mContext;
    private ConverstionPOJO convoMessage;
    private List<ConverstionPOJO> mMessageList;
    private DeleteMessage deleteMessage;
    private ImageView roundTick1, roundTick2;
    private Retry retry;

    public MessageListAdapter(Activity context, List<ConverstionPOJO> messageList, DeleteMessage deleteMessage, Retry retry) {
        mContext = context;
        mMessageList = messageList;
        this.deleteMessage = deleteMessage;
        this.retry = retry;
    }

    public void updateData(List<ConverstionPOJO> messageList) {
        this.mMessageList = messageList;
        roundTick1.setVisibility(View.VISIBLE);
        roundTick2.setVisibility(View.VISIBLE);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }

    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType(int position) {
        ConverstionPOJO message = mMessageList.get(position);
        this.convoMessage = message;
        if (message.isQuestion()) {
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    // Inflates the appropriate layout according to the ViewType.
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_sent_layout, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }
        return null;
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final ConverstionPOJO message = mMessageList.get(position);
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message.getMessages());
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);
        }
        if (message.isPaymentFailed()) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    retry.retry(message.getMessages(), position);
                }
            });
        }
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                deleteMessage.deleteMessage(message.getId(), position, message.getType());
                if (holder.getItemViewType() == VIEW_TYPE_MESSAGE_SENT) {
                    ((SentMessageHolder) holder).roundTickSent.setVisibility(View.VISIBLE);

                } else {
                    ((ReceivedMessageHolder) holder).roundTickReceived.setVisibility(View.VISIBLE);

                }
                if (message.isFlag()) {
                    if (holder.getItemViewType() == VIEW_TYPE_MESSAGE_SENT) {
                        ((SentMessageHolder) holder).roundTickSent.setVisibility(View.GONE);

                    } else {
                        ((ReceivedMessageHolder) holder).roundTickReceived.setVisibility(View.GONE);

                    }

                }
                return true;
            }
        });

    }

    public void setRoundVisible() {
        roundTick1.setVisibility(View.VISIBLE);
        roundTick2.setVisibility(View.VISIBLE);
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText;
        ImageView roundTickSent;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText = (TextView) itemView.findViewById(R.id.text_message_body);
            timeText = (TextView) itemView.findViewById(R.id.text_message_time);
            roundTickSent = itemView.findViewById(R.id.round_tick);
            roundTick1 = roundTickSent;
        }

        void bind(String message) {
            if (message == null || message.isEmpty()) {
                messageText.setVisibility(View.GONE);
            }
            if (!convoMessage.isPaymentFailed()) {
                timeText.setVisibility(View.GONE);
            } else {
                timeText.setText("PAYMENT FAILED! CLICK TO RETRY ");
            }
            messageText.setText(message);

            // Format the stored timestamp into a readable String using method.
            //timeText.setText(Utils.formatDateTime(message.getCreatedAt()));
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        TextView messageText, timeText, nameText;
        ImageView profileImage, roundTickReceived;
        RatingBar rating;
        // RatingBar ratingBar;

        ReceivedMessageHolder(View itemView) {
            super(itemView);
            messageText = itemView.findViewById(R.id.text_message_body);
            roundTickReceived = itemView.findViewById(R.id.round_tick);
            // timeText = itemView.findViewById(R.id.text_message_time);
            nameText = itemView.findViewById(R.id.text_message_name);
            profileImage = itemView.findViewById(R.id.image_message_profile);
            rating = itemView.findViewById(R.id.rb_rating);
            roundTick2 = roundTickReceived;


            // ratingBar = itemView.findViewById(R.id.rb_rating);
           /* ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                @Override
                public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                    JsonObject jsonObject = new JsonObject();
                    jsonObject.addProperty("Id", mMessageList.get(getAdapterPosition()).getId());
                    jsonObject.addProperty("Rating", (int) v);

                    Network.postRating(new NetworkResponse() {
                        @Override
                        public void onSuccess(String data) {
                            Gson gson = new Gson();
                            Rating rating = gson.fromJson(data, Rating.class);
                            if (rating.getMeta().getStatus().equalsIgnoreCase("Ok")) {
                                Toast.makeText(mContext, "Rating posted Successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(mContext, "Please check your internet connection", Toast.LENGTH_SHORT).show();

                            }
                        }

                        @Override
                        public void onError(String data) {

                        }
                    }, jsonObject, mContext);
                }
            });
        */

        }

        void bind(final ConverstionPOJO message) {
            if (message == null) {
                //   ratingBar.setVisibility(View.GONE);
                messageText.setVisibility(View.GONE);
                nameText.setVisibility(View.GONE);
                profileImage.setVisibility(View.GONE);
                // timeText.setVisibility(View.GONE);
            } else {
                //   ratingBar.setVisibility(View.VISIBLE);
                messageText.setVisibility(View.VISIBLE);
                nameText.setVisibility(View.VISIBLE);
                profileImage.setVisibility(View.VISIBLE);
                //timeText.setVisibility(View.VISIBLE);
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                messageText.setText(Html.fromHtml(message.getMessages(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                messageText.setText(Html.fromHtml(message.getMessages()));
            }
            // messageText.setText(message.getMessages());

            // Format the stored timestamp into a readable String using method.
            // timeText.setText(Utils.formatDateTime(message.getCreatedAt()));
            if (message.getAstrologerName() == null) {
                if (message.getModeratorName() != null) {
                    nameText.setText(message.getModeratorName());
                } else {
                    nameText.setText("AstroSine");
                }
                if (message.getModeratorImage() != null) {
                    Picasso.get().load(message.getModeratorImage())
                            .placeholder(R.drawable.ic_launcher)
                            //.transform(new CircleTransform())
                            .into(profileImage);
                } else {
                    profileImage.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_launcher));
                }
            } else {
                if (message.getAstrologerName() != null) {
                    nameText.setText(message.getAstrologerName());
                } else {
                    nameText.setText("AstroSine");
                }
                if (message.getAstrologerImage() != null) {
                    Picasso.get().load(message.getAstrologerImage())
                            .placeholder(R.drawable.ic_launcher)
                            //.transform(new CircleTransform())
                            .into(profileImage);
                } else {
                    profileImage.setImageDrawable(ContextCompat.getDrawable(mContext, R.mipmap.ic_launcher));
                }
            }

            if (message.getType().equals("answer")){
                rating.setRating(message.getRating());
                rating.setVisibility(View.VISIBLE);
                rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("user_id", SharedPreferencesManager.getUserId());
                            jsonObject.put("rate", (int)(v));
                            Networking.postRequest(new NetworkResponse() {
                                @Override
                                public void onSuccess(String response) {
                                    Toast.makeText(mContext, "Thanks for your rating", Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onError(String response) {

                                }
                            }, jsonObject, Constants.RATE + message.getAnswerId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                });
            }else{
                rating.setVisibility(View.GONE);
            }

           /* if (mMessageList.size() <= 1){
                ratingBar.setVisibility(View.GONE);
            }else if (getAdapterPosition() == mMessageList.size() - 1) {
                ratingBar.setVisibility(View.VISIBLE);
            } else {
                ratingBar.setVisibility(View.GONE);
            }*/
//            if(getAdapterPosition() == mMessageList.size() - 1){
//                ratingBar.setVisibility(View.INVISIBLE);
//                messageText.setVisibility(View.INVISIBLE);
//                nameText.setVisibility(View.INVISIBLE);
//                profileImage.setVisibility(View.INVISIBLE);
//                timeText.setVisibility(View.INVISIBLE);
//            }
            // Insert the profile image from the URL into the ImageView.
            //  Utils.displayRoundImageFromUrl(mContext, message.getSender().getProfileUrl(), profileImage);
        }
    }

    public interface DeleteMessage {
        void deleteMessage(int id, int position, String type);

    }

    public interface Retry {
        void retry(String retry, int position);
    }
}