package com.astrosine.app.Firebase;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.astrosine.app.ApplicationClass;
import com.astrosine.app.MainActivity;
import com.astrosine.app.R;
import com.astrosine.app.SQLiteDatabase.AstroDatabase;
import com.astrosine.app.Utility.NotificationEvent;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    AstroDatabase astroDatabase;

    public MyFirebaseMessagingService() {
        astroDatabase = new AstroDatabase(ApplicationClass.getContext());
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.d("nirajan","token : "+ s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        System.out.println(remoteMessage.getData().toString());
        notifyThis(remoteMessage.getData().get("title"), remoteMessage.getData().get("body"));
    }

    public void notifyThis(String title, String message) {
        EventBus.getDefault().post(new NotificationEvent(0));

        NotificationManager notificationManager = (NotificationManager) this
                .getSystemService(Activity.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(
                this.getApplicationContext(), MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent pIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setContentTitle(title)
                .setContentText(message)
                .setDefaults(
                        Notification.DEFAULT_SOUND
                                | Notification.DEFAULT_VIBRATE)
                .setContentIntent(pIntent).setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher).build();
        if (notificationManager != null) {
            notificationManager.notify(2, notification);
        }

//        NotificationCompat.Builder b = new NotificationCompat.Builder(this);
//        b.setAutoCancel(true)
//                .setDefaults(NotificationCompat.DEFAULT_ALL)
//                .setWhen(System.currentTimeMillis())
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle(title)
////                .setTicker("{your tiny message}")
//                .setContentInfo("INFO")
//                .setContentText(message);
//
//        NotificationManager nm = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
//
//        Intent intent = new Intent(this, MainActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
//                intent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//
//        b.setContentIntent(contentIntent);
//        if (nm != null) {
//            nm.notify(1, b.build());
//        }
    }
}

