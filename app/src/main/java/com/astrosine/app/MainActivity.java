package com.astrosine.app;

import android.app.Dialog;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.astrosine.app.AboutUs.AboutUsFragment;
import com.astrosine.app.AstrologersList.AstrologersList;
import com.astrosine.app.AstrologersList.AstrologersListModel;
import com.astrosine.app.AstrologersList.AstrologersViewAdapter;
import com.astrosine.app.Chat.ConverstionPOJO;
import com.astrosine.app.Chat.MessageListAdapter;
import com.astrosine.app.Constants.Constants;
import com.astrosine.app.Help.HelpFragment;
import com.astrosine.app.Networking.NetworkResponse;
import com.astrosine.app.Networking.Networking;
import com.astrosine.app.Notification.Notification;
import com.astrosine.app.RegisterFragment.RegisterFragment;
import com.astrosine.app.SQLiteDatabase.AstroDatabase;
import com.astrosine.app.Support.SupportFragment;
import com.astrosine.app.Utility.NotificationEvent;
import com.astrosine.app.Utility.SharedPreferencesManager;
import com.astrosine.app.Utility.Utility;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener, BillingProcessor.IBillingHandler {
    private FragmentManager framentManager;
    private List<ConverstionPOJO> messageList;
    private RelativeLayout progressLayout;
    private TextView userName;
    private TextView dob, text1, text2, price;
    private AstroDatabase astroDatabase;
    private String question = null;
    private MessageListAdapter messageListAdapter;
    private LinearLayout button;
    private EditText messageBox;
    private NestedScrollView scrollView;
    private RecyclerView recyclerView;
    private ImageView navImage, notification;
    private ProgressBar progress_main;
    private Toolbar toolbar;
    private ImageView notificationIcon;
    private BillingProcessor mBillProcessor;

    private String path = "/data/user/0/com.astrorule.app/app_imageDir";
    // private BillingClient billingClient;
    private AstrologersViewAdapter.chooseAstrologer chooseAstrologer;
//    private BillingClient billingClient;
    private String type, message, token;
    private int id;
    private MessageListAdapter.DeleteMessage deleteMessageInstance;
    private Button delete, cancel;
    private LinearLayout layoutDelete;
    private ArrayList<Integer> positions = new ArrayList<>();
    private ArrayList<Integer> ids = new ArrayList<>();
    private ArrayList<String> delType = new ArrayList<>();
    private RelativeLayout progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferencesManager.setFirstTime(true);
        ImageView splash = findViewById(R.id.iv_splash);
        Picasso.get().load(R.drawable.splash_photo).fit().into(splash);
        splash();


        progressBar = findViewById(R.id.progress_bar);
//        inAppPurchase(true);
        mBillProcessor = new BillingProcessor(this, getResources().getString(R.string.app_key_playstore), this);
        mBillProcessor.initialize();

//        price.setText(mBillProcessor.getPurchaseListingDetails("first_question_1.0").priceText);
        delete = findViewById(R.id.btn_delete);
        cancel = findViewById(R.id.btn_cancel);

        delete.setOnClickListener(this);
        cancel.setOnClickListener(this);
        layoutDelete = findViewById(R.id.delete);

        toolbar = findViewById(R.id.toolbar);
        notificationIcon = toolbar.findViewById(R.id.notification);
        notificationIcon.setOnClickListener(this);
        setTheme(R.style.AppTheme);
        astroDatabase = new AstroDatabase(this);
        progress_main = findViewById(R.id.progress_main);
        button = findViewById(R.id.button_chatbox_send);
        button.setOnClickListener(this);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        scrollView = findViewById(R.id.scroll_view);
        scrollView.setNestedScrollingEnabled(true);
        messageList = new ArrayList<>();

        text1.setText(R.string.about_para1);
        text2.setText(R.string.about_para2);
        progressLayout = findViewById(R.id.progress_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        final AVLoadingIndicatorView progressBar = toolbar.findViewById(R.id.indicator);
        notification = toolbar.findViewById(R.id.notification);
        progressBar.setVisibility(View.GONE);
        notification.setVisibility(View.VISIBLE);
        messageBox = findViewById(R.id.edittext_chatbox);
        messageBox.setOnClickListener(this);
        if (getIntent().getStringExtra("question") != null) {
            question = getIntent().getStringExtra("question");
            messageBox.setText(question);
        }

        TextView mainText = findViewById(R.id.mainText);
        mainText.setText(" \"YOU'RE LIVE WITH ASTROSINE\" ");
        TextView title = toolbar.findViewById(R.id.title);
        title.setText("AstroSine");
        framentManager = getSupportFragmentManager();
        final DrawerLayout drawer = findViewById(R.id.drawer_layout);

        /*
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        */

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.bringToFront();

        View hView = navigationView.getHeaderView(0);
        navImage = hView.findViewById(R.id.imageView);
        price = hView.findViewById(R.id.price);
        price.setText(SharedPreferencesManager.getPrice());
        userName = hView.findViewById(R.id.tv_name);
        dob = hView.findViewById(R.id.tv_dob);
        hView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterFragment registerFragment = new RegisterFragment();
                framentManager.beginTransaction().replace(R.id.content_main, registerFragment).addToBackStack("Register").commit();
            }
        });

        ImageView navigation = toolbar.findViewById(R.id.navigation);
        navigation.setImageDrawable(getResources().getDrawable(R.drawable.ic_menu));
        navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.END);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                    Utility.hideKeyboard(MainActivity.this);
                }
            }
        });

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.home) {
        } else if (id == R.id.about) {
            AboutUsFragment supportFragment = new AboutUsFragment();
            framentManager.beginTransaction().replace(R.id.content_main, supportFragment).addToBackStack("About").commit();
        } else if (id == R.id.astrologers_list) {
            AstrologersList astrologersList = new AstrologersList();
            framentManager.beginTransaction().replace(R.id.content_main, astrologersList).addToBackStack("AstrologersList").commit();
        } else if (id == R.id.help) {
            HelpFragment helpFragment = new HelpFragment();
            framentManager.beginTransaction().replace(R.id.content_main, helpFragment).addToBackStack("Help").commit();
        } else if (id == R.id.my_account) {
            RegisterFragment registerFragment = new RegisterFragment();
            framentManager.beginTransaction().replace(R.id.content_main, registerFragment).addToBackStack("Register").commit();
        } else if (id == R.id.support) {
            SupportFragment supportFragment = new SupportFragment();
            framentManager.beginTransaction().replace(R.id.content_main, supportFragment).addToBackStack("Support").commit();
        } else if (id == R.id.terms) {
            Intent terms = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.contract)));
            startActivity(terms);
        } else if (id == R.id.privacy) {
            Intent privacy = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.terms)));
            startActivity(privacy);
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void splash() {
        JSONObject jsonObject = new JSONObject();
        Log.d("nirajan", "device id :" + SharedPreferencesManager.getDeviceId());
        Log.d("nirajan", "gcmid : " + SharedPreferencesManager.getGcmId());

        try {
            jsonObject.put("device_token", SharedPreferencesManager.getDeviceId());
            jsonObject.put("gcm_id", SharedPreferencesManager.getGcmId());
            jsonObject.put("app_version", "1.0.1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        NetworkResponse networkResponse = new NetworkResponse() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject responseObject = new JSONObject(response);
                    JSONObject status = responseObject.getJSONObject("status");
                    int responseCode = status.getInt("responseCode");
                    String message = status.getString("message");
                    if (responseCode == 200 && message.equalsIgnoreCase("User Exists")) {

                        JSONObject body = responseObject.getJSONObject("body");
                        JSONObject putObject = new JSONObject();
                        putObject.put("id", body.getString("id"));
                        putObject.put("user_id", body.getString("user_id"));
                        Log.d("nirajan", "user Id : " + body.getString("user_id"));
                        SharedPreferencesManager.setUserId(body.getString("user_id"));
                        populateChat();
                        putObject.put("user_code", body.getString("user_code"));
                        putObject.put("name", body.getString("name"));
                        putObject.put("image", body.getString("image"));
                        putObject.put("gender", body.getString("gender"));
                        putObject.put("country", body.getString("country"));
                        putObject.put("state", body.getString("state"));
                        putObject.put("location", body.getString("location"));
                        putObject.put("device_token", body.getString("device_token"));
                        SharedPreferencesManager.setDeviceId(body.getString("device_token"));
                        putObject.put("app_version", body.getString("app_version"));
                        putObject.put("gcm_id", body.getString("gcm_id"));
                        putObject.put("device_type", body.getString("device_type"));
                        putObject.put("date_of_birth", body.getString("date_of_birth"));
                        putObject.put("time_of_birth", body.getString("time_of_birth"));
                        putObject.put("time_accuracy", body.getInt("time_accuracy"));
                        putObject.put("is_free", body.getInt("is_free"));
                        putObject.put("flag", body.getInt("flag"));
                        putObject.put("questionCount", body.getInt("questionCount"));
                        putObject.put("package_id", body.getInt("package_id"));
                        putObject.put("free_count", body.getInt("free_count"));
                        putObject.put("status", body.getInt("status"));
                        putObject.put("created_at", body.getString("created_at"));
                        putObject.put("updated_at", body.getString("updated_at"));
                        putObject.put("image_url", body.getString("image_url"));
                        putObject.put("email", body.getString("email"));
                        putObject.put("upgrade", body.getBoolean("upgrade"));
                        putObject.put("PRICE", body.getString("PRICE"));
                        putObject.put("welcome_message", body.getString("welcome_message"));
                        putObject.put("first_msg", body.getString("first_msg"));
                        putObject.put("count", body.getInt("count"));
                        SharedPreferencesManager.setUserData(putObject.toString());
                        SharedPreferencesManager.setRegistered(true);
                        SharedPreferencesManager.setImageUrl(body.getString("image_url"));
                        Picasso.get().load(body.getString("image_url")).into(navImage);
                        text1.setText(Html.fromHtml(body.getString("welcome_message")));
                        text2.setText(Html.fromHtml(body.getString("first_msg")));
                        SharedPreferencesManager.setImageUrl(body.getString("image_url"));
                        try {
                            userName.setText(body.getString("name"));
                            dob.setText(body.getString("date_of_birth"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        text1.setText(R.string.about_para1);
                        text2.setText(R.string.about_para2);
                /*        RegisterFragment registerFragment = new RegisterFragment();
                        framentManager.beginTransaction().replace(R.id.content_main, registerFragment).addToBackStack("Register").commit();
                 */
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressLayout.setVisibility(View.GONE);
            }

            @Override
            public void onError(String response) {
                progressLayout.setVisibility(View.GONE);
            }
        };

        if (question == null) {
            Networking.postRequest(networkResponse, jsonObject, Constants.SPLASH);
        } else {
            progressLayout.setVisibility(View.GONE);
        }
    }

    /*public void setChatData() {
        //  messageList = astroDatabase.getChatData();
        recyclerView = findViewById(R.id.reyclerview_message_list);
        messageListAdapter = new MessageListAdapter(this, messageList);
        recyclerView.setAdapter(messageListAdapter);
        recyclerView.hasNestedScrollingParent();
        scrollView.fullScroll(NestedScrollView.FOCUS_DOWN);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.scrollToPosition(messageListAdapter.getItemCount());
        linearLayoutManager.canScrollVertically();
        recyclerView.setLayoutManager(linearLayoutManager);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        }, 1000);
        try {
            recyclerView.smoothScrollToPosition(messageListAdapter.getItemCount());
        } catch (Exception e) {

        }

    }*/

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(NotificationEvent event) {
        int data = event.getData();
        populateChat();
    }

    public void populateChat() {
        if (SharedPreferencesManager.getUserId()!=null) {
            NetworkResponse networkResponse = new NetworkResponse() {
                @Override
                public void onSuccess(String response) {
                    try {
                        layoutDelete.setVisibility(View.GONE);
                        messageList.clear();
                        Log.d("nirjan", "response chat data " + response);
                        final JSONObject jsonObject = new JSONObject(response);
                        JSONObject status = jsonObject.getJSONObject("status");
                        int responseCode = status.getInt("responseCode");
                        if (responseCode == 200) {
                            JSONArray body = jsonObject.getJSONArray("body");
                            for (int i = 0; i < body.length(); i++) {
                                JSONObject data = body.getJSONObject(i);
                                type = data.getString("type");
                                message = data.getString("message");
                                id = data.getInt("id");
                                int answerId = data.optInt("answer_id");
                                int rating = data.getInt("rating");
                                boolean isQuestion = false;
                                boolean paymentFailed = false;
                                if (type.equalsIgnoreCase("question")) {
                                    isQuestion = true;
                                }
                                if (type.equalsIgnoreCase("payment_failed")) {
                                    paymentFailed = true;
                                    isQuestion = true;
                                }
                                ConverstionPOJO converstionPOJO = new ConverstionPOJO(message, isQuestion, paymentFailed, id, type, false, rating, answerId);
                                messageList.add(converstionPOJO);
                            }
                            delType.clear();
                            positions.clear();
                            ids.clear();
                            recyclerView = findViewById(R.id.reyclerview_message_list);

                            MessageListAdapter.DeleteMessage deleteMessageInstance = new MessageListAdapter.DeleteMessage() {
                                @Override
                                public void deleteMessage(int id, int position, String type) {
                                    positions.add(position);
                                    ids.add(id);
                                    delType.add(type);
                                    layoutDelete.setVisibility(View.VISIBLE);
                                }
                            };

                            MessageListAdapter.Retry retry = new MessageListAdapter.Retry() {
                                @Override
                                public void retry(String retry, int position) {
                                    messageBox.setText(retry);
                                }
                            };
                            messageListAdapter = new MessageListAdapter(MainActivity.this, messageList, deleteMessageInstance, retry);
                            recyclerView.setAdapter(messageListAdapter);
                            recyclerView.hasNestedScrollingParent();
                            scrollView.fullScroll(NestedScrollView.FOCUS_DOWN);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this);
                            linearLayoutManager.scrollToPosition(messageListAdapter.getItemCount());
                            linearLayoutManager.canScrollVertically();
                            recyclerView.setLayoutManager(linearLayoutManager);
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    scrollView.fullScroll(View.FOCUS_DOWN);
                                }
                            }, 1000);
                            try {
                                recyclerView.scrollToPosition(messageListAdapter.getItemCount());
                            } catch (Exception e) {

                            }

                            // setChatData();
                        }

                        //     messageList.add()
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onError(String response) {

                }
            };

            Networking.getRequest(networkResponse, this, Constants.CHAT + SharedPreferencesManager.getUserId());
            Log.d("nirajan", " url : " + Constants.CHAT + SharedPreferencesManager.getUserId());
        }
    }

    public void deleteData(final ArrayList<Integer> positions, ArrayList<Integer> ids, ArrayList<String> delType) {
        final JSONObject jsonObject = new JSONObject();

        NetworkResponse networkResponse = new NetworkResponse() {
            @Override
            public void onSuccess(String response) {
                populateChat();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String response) {
                Toast.makeText(MainActivity.this, "Failed", Toast.LENGTH_LONG).show();
                populateChat();
                progressBar.setVisibility(View.GONE);
            }
        };
        try {
            jsonObject.put("user_id", SharedPreferencesManager.getUserId());

            JSONArray jsonArray = new JSONArray();
            for (int a = 0; a < ids.size(); a++) {
                JSONObject jsonInside = new JSONObject();
                jsonInside.put("id", ids.get(a));
                jsonInside.put("type", delType.get(a));
                jsonArray.put(jsonInside);
            }
            jsonObject.put("chat_history_id_list", jsonArray);
            Log.d("nirajan", "json :  " + jsonObject.toString());
            Networking.postRequest(networkResponse, jsonObject, Constants.DELETECHAT);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void insertChatData(boolean paymentFailed, String type) {
        // astroDatabase.insertChatTable(9, messageBox.getText().toString(), "true");
        //populateChat();
       /* ConverstionPOJO converstionPOJO = new ConverstionPOJO(messageBox.getText().toString(), true, paymentFailed, 0,type);
        messageList.add(converstionPOJO);
        messageListAdapter.updateData(messageList);
        messageBox.setText("");*/
        scrollView.fullScroll(View.FOCUS_DOWN);
        recyclerView.smoothScrollToPosition(messageListAdapter.getItemCount());
        recyclerView.hasNestedScrollingParent();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(View.FOCUS_DOWN);
            }
        }, 500);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_chatbox_send) {
            if (!SharedPreferencesManager.getRegistered()) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.dialog_error);
                TextView message = dialog.findViewById(R.id.message);
                message.setText("You need to be a Registered user to ask Questions.");
                dialog.findViewById(R.id.btn_done).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        RegisterFragment registerFragment = new RegisterFragment();
                        framentManager.beginTransaction().replace(R.id.content_main, registerFragment).addToBackStack("Register").commit();
                    }
                });
                dialog.show();
            }else if (messageBox.getText().toString().isEmpty()) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.dialog_error);
                TextView message = dialog.findViewById(R.id.message);
                message.setText("You can not post an empty Question.");
                dialog.findViewById(R.id.btn_done).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialog.show();

//                Toast.makeText(MainActivity.this, " Empty Message ", Toast.LENGTH_SHORT).show();
            } else {
                if (SharedPreferencesManager.getDefaultId()) {
                    final Dialog prefferdDialog = new Dialog(MainActivity.this);
                    prefferdDialog.show();
                    Window window = prefferdDialog.getWindow();
                    window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    prefferdDialog.setContentView(R.layout.dialog_preffered_astrologers);
                    final RadioGroup radioGroup = prefferdDialog.findViewById(R.id.radio_group);
                    final Button done = prefferdDialog.findViewById(R.id.btn_done);
                    done.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int radioButtonId = radioGroup.getCheckedRadioButtonId();
                            final RadioButton chooseButton = prefferdDialog.findViewById(radioButtonId);
                            if (chooseButton != null) {
                                if ((chooseButton.getText().toString().equalsIgnoreCase(getResources().getString(R.string.use_default_astrologer)))) {
                                    prefferdDialog.dismiss();
                                    inAppPurchase(false);
                                } else if (chooseButton.getText().toString().equalsIgnoreCase(getResources().getString(R.string.choose_new_astrologer))) {
                                    astrologersList();
                                    prefferdDialog.dismiss();
                                } else {
                                    prefferdDialog.dismiss();
                                    inAppPurchase(false);
                                }
                            } else {
                                prefferdDialog.dismiss();
                                inAppPurchase(false);
                            }
                        }
                    });

                } else {
                    astrologersList();
                }
            }
        } else if (v.getId() == R.id.notification) {
            Notification notification = new Notification();
            framentManager.beginTransaction().replace(R.id.content_main, notification).addToBackStack("Notification").commit();
        } else if (v.getId() == R.id.btn_delete) {
            deleteData(positions, ids, delType);
            progressBar.setVisibility(View.VISIBLE);
           /* for (int i = 0; i < positions.size(); i++) {
                messageList.remove(positions.get(i).intValue());
                messageList.size();
            }
            messageListAdapter.updateData(messageList);
            positions.clear();
            ids.clear();
            layoutDelete.setVisibility(View.GONE);
            */
        } else if (v.getId() == R.id.btn_cancel) {
            layoutDelete.setVisibility(View.GONE);
            populateChat();

            // progressBar.setVisibility(View.VISIBLE);
           /* messageListAdapter.updateData(messageList);
           positions.clear();
            ids.clear();*/

            //   deleteData(positions, ids);
            /*
            positions.clear();
            ids.clear();
            layoutDelete.setVisibility(View.GONE);
            messageListAdapter.setRoundVisible();*/
        }/* else if (v.getId() == R.id.edittext_chatbox) {
            if (SharedPreferencesManager.getUserId() == null) {
                RegisterFragment registerFragment = new RegisterFragment();
                framentManager.beginTransaction().replace(R.id.content_main, registerFragment).addToBackStack("Register").commit();

            }
        }*/
    }

    public void inAppPurchase(boolean isFirst){
        mBillProcessor.purchase(MainActivity.this, "first_question_1.0");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!mBillProcessor.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


///*    public void inAppPurchase(final boolean firstTime) {
//        bp = new BillingProcessor(this, getResources().getString(R.string.app_key_playstore), this);
//        bp.initialize();
//        billingClient = BillingClient.newBuilder(this).setListener(this).enablePendingPurchases().build();
//        billingClient.startConnection(new BillingClientStateListener() {
//            @Override
//            public void onBillingSetupFinished(BillingResult billingResult) {
//                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
//
//                    List<String> skuList = new ArrayList<>();
//                    skuList.add("first_question_1.0");
//                    SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
//                    params.setSkusList(skuList).setType(BillingClient.SkuType.INAPP);
//                    billingClient.querySkuDetailsAsync(params.build(),
//                            new SkuDetailsResponseListener() {
//                                @Override
//                                public void onSkuDetailsResponse(BillingResult billingResult,
//                                                                 List<SkuDetails> skuDetailsList) {
//                                    // Process the result.
//                                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK && skuDetailsList != null) {
//                                        for (SkuDetails skuDetails : skuDetailsList) {
//                                            String sku = skuDetails.getSku();
//                                            String skuprice = skuDetails.getPrice();
//                                            SharedPreferencesManager.setPrice(skuprice);
//                                            price.setText(skuprice);
//                                            if ("first_question_1.0".equals(sku)) {
//                                                //   premiumUpgradePrice = price;
//                                            }
//                                            if (!firstTime) {
//                                                BillingFlowParams flowParams = BillingFlowParams.newBuilder()
//                                                        .setSkuDetails(skuDetailsList.get(0)).build();
//                                                BillingResult responseCode = billingClient.launchBillingFlow(MainActivity.this, flowParams);
//                                            }
//                                        }
//                                    }
//                                }
//                            });
//                }
//            }
//
//            @Override
//            public void onBillingServiceDisconnected() {
//                // Try to restart the connection on the next request to
//                // Google Play by calling the startConnection() method.
//            }
//        });
//        if (!firstTime) {
//            ConsumeParams consumeParams = ConsumeParams.newBuilder()
//                    .setPurchaseToken(getResources().getString(R.string.app_key_playstore))
//                    //.setDeveloperPayload(/* payload */)
//                    .build();
//
//            ConsumeResponseListener listener = new ConsumeResponseListener() {
//                @Override
//                public void onConsumeResponse(BillingResult billingResult, String outToken) {
//                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
//                        // Handle the success of the consume operation.
//                        // For example, increase the number of coins inside the user's basket.
//                        token = outToken;
//                        Toast.makeText(MainActivity.this, "Purchased item", Toast.LENGTH_LONG).show();
//                        question();
//                    } else {
//                        paymentFailure();
//                    }
//                }
//            };
//            billingClient.consumeAsync(consumeParams, listener);
//        }
//    }

    public void question(final TransactionDetails details) {
        NetworkResponse networkResponse = new NetworkResponse() {
            @Override
            public void onSuccess(String response) {
                progress_main.setVisibility(View.GONE);
                Log.d("nirajan", "question" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject status = jsonObject.getJSONObject("status");
                    int responseCode = status.getInt("responseCode");
                    if (responseCode == 200) {
                        mBillProcessor.consumePurchase(details.purchaseInfo.purchaseData.productId);

                        populateChat();
                        messageBox.setText("");
                        //      Toast.makeText(MainActivity.this, "Question Sent", Toast.LENGTH_LONG).show();
                    } else {
                        messageBox.setText("");
                        Toast.makeText(MainActivity.this, "Unable to send question", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    progress_main.setVisibility(View.GONE);
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String response) {
                messageBox.setText("");
                progress_main.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, "Internal error ", Toast.LENGTH_LONG).show();
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", SharedPreferencesManager.getUserId());
            jsonObject.put("question", messageBox.getText().toString());
            jsonObject.put("transaction_id", details.purchaseInfo.purchaseData.purchaseToken);
            jsonObject.put("astrologer_id", SharedPreferencesManager.getAstrologersId());
            System.out.println(jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Networking.postRequest(networkResponse, jsonObject, Constants.QUESTIONS);
    }

    private void paymentFailure() {
        NetworkResponse networkResponse = new NetworkResponse() {
            @Override
            public void onSuccess(String response) {
                progress_main.setVisibility(View.GONE);
                Log.d("nirajan", "question" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject status = jsonObject.getJSONObject("status");
                    int responseCode = status.getInt("responseCode");
                    if (responseCode == 200) {
                        //insertChatData(true,"payment_failed");
                        populateChat();
                        messageBox.setText("");
                        Toast.makeText(MainActivity.this, "PAYMENT FAILED", Toast.LENGTH_LONG).show();
                    } else {
                        messageBox.setText("");
                        Toast.makeText(MainActivity.this, "Unable to send question", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    progress_main.setVisibility(View.GONE);
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String response) {
                messageBox.setText("");
                progress_main.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, "Internal error ", Toast.LENGTH_LONG).show();
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("userid", SharedPreferencesManager.getUserId());
            jsonObject.put("message", messageBox.getText());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Networking.postRequest(networkResponse, jsonObject, Constants.PAYMENTFAILURE);
    }


//    @Override
//    public void onPurchasesUpdated(BillingResult
//                                           billingResult, @Nullable List<Purchase> purchases) {
//        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
//            Toast.makeText(this, "Purchased item", Toast.LENGTH_LONG).show();
//        } else {
//            Toast.makeText(this, " FAILED", Toast.LENGTH_LONG).show();
//        }
//    }

    public void astrologersList() {
        final ArrayList<AstrologersListModel> arrayListModel = new ArrayList();

        final Dialog dialog = new Dialog(MainActivity.this);
        dialog.show();
        dialog.setContentView(R.layout.dialog_choose_astrologers);
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Button cancel = dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                inAppPurchase(false);
            }
        });
        final AstrologersViewAdapter.chooseAstrologer chooseAstrologer = new AstrologersViewAdapter.chooseAstrologer() {
            @Override
            public void chooseAstrologer(String id) {
                SharedPreferencesManager.setAstrologerId(id);
                dialog.dismiss();
                SharedPreferencesManager.setDefaultId(true);
                inAppPurchase(false);
            }
        };
        final RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        final AstrologersViewAdapter astrologersAdapter = new AstrologersViewAdapter(arrayListModel, chooseAstrologer);
        recyclerView.setAdapter(astrologersAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(true);

        NetworkResponse networkResponse2 = new NetworkResponse() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject status = jsonObject.getJSONObject("status");
                    int responseCode = status.getInt("responseCode");
                    JSONArray body = jsonObject.getJSONArray("body");
                    if (responseCode == 200) {
                        for (int i = 0; i < body.length(); i++) {
                            JSONObject data = body.getJSONObject(i);
                            AstrologersListModel astrologersListModel = new AstrologersListModel();
                            astrologersListModel.setAbout(data.getString("about"));
                            astrologersListModel.setImageUrl(data.getString("image"));
                            astrologersListModel.setName(data.getString("name"));
                            astrologersListModel.setId(data.getString("id"));
                            arrayListModel.add(astrologersListModel);
                        }
                        astrologersAdapter.notifyDataSetChanged();
                        recyclerView.setVisibility(View.VISIBLE);
                        dialog.findViewById(R.id.progress_main).setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String response) {
                Toast.makeText(MainActivity.this, "Unable to send question", Toast.LENGTH_LONG).show();
            }
        };
        Networking.getRequest(networkResponse2, MainActivity.this, Constants.ASTROLOGERSLIST);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBillProcessor != null)
            mBillProcessor.release();
//        billingClient.endConnection();
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        question(details);
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        paymentFailure();
    }

    @Override
    public void onBillingInitialized() {
        price.setText(mBillProcessor.getPurchaseListingDetails("first_question_1.0").currency + " " + mBillProcessor.getPurchaseListingDetails("first_question_1.0").priceText);
//        System.out.println(mBillProcessor.getPurchaseListingDetails("first_question_1.0").);
//        System.out.println(mBillProcessor.isInitialized());
    }
}

/*  public String saveImage(Bitmap myBitmap) {

 *//* if(imageFile!=null){

            req = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("medicine", name.getText().toString())
                    .addFormDataPart("token", mGlobalVariable.getUserData(DataHolder.TOKEN))
                    .addFormDataPart("quantity", quantity.getText().toString())
                    .addFormDataPart("photo", "order.png", RequestBody.create(MEDIA_TYPE_PNG, imageFile)).build();

        }else{

            req = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("medicine", name.getText().toString())
                    .addFormDataPart("token", mGlobalVariable.getUserData(DataHolder.TOKEN))
                    .addFormDataPart("quantity", quantity.getText().toString()).build();

        }*//*

        ContextWrapper cw = new ContextWrapper(this);
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "profile.png");
        //  imageFile = BitmapFactory.decodeFile(directory + "profile.jpg");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            myBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.d(Utility.TAG, " nirajan " + directory.getAbsolutePath());
        return directory.getAbsolutePath();
    }*/

