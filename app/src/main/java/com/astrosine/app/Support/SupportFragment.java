package com.astrosine.app.Support;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.astrosine.app.Notification.Notification;
import com.astrosine.app.R;
import com.wang.avi.AVLoadingIndicatorView;

public class SupportFragment extends Fragment implements View.OnClickListener {

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button mail = view.findViewById(R.id.mail);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        Resources.Theme theme = getActivity().getTheme();
        ImageView backbutton = toolbar.findViewById(R.id.navigation);
        final AVLoadingIndicatorView progressBar=toolbar.findViewById(R.id.indicator);
        progressBar.setVisibility(View.GONE);
        final ImageView notification = toolbar.findViewById(R.id.notification);
        notification.setVisibility(View.VISIBLE);
        final FragmentManager  framentManager = getActivity().getSupportFragmentManager();
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Notification notification = new Notification();
                framentManager.beginTransaction().replace(R.id.content_main, notification).addToBackStack("Notification").commit();
            }
        });
        backbutton.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        TextView title = toolbar.findViewById(R.id.title);
        title.setText("SUPPORT");
        mail.setOnClickListener(this);
        Button visit = view.findViewById(R.id.visit);
        visit.setOnClickListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_support, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mail:
                String[] recipitants = {getResources().getString(R.string.email)};
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.putExtra(Intent.EXTRA_EMAIL, recipitants);
                //  intent.setData(Uri.parse("mailto:"+recipitants));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setType("message/rfc822");
                getActivity().startActivity(Intent.createChooser(intent, "Email"));
                break;
            case R.id.visit:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.website)));
                startActivity(browserIntent);
                break;
        }

    }
}
