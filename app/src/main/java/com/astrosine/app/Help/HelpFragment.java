package com.astrosine.app.Help;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.astrosine.app.Constants.Constants;
import com.astrosine.app.MainActivity;
import com.astrosine.app.Networking.NetworkResponse;
import com.astrosine.app.Networking.Networking;
import com.astrosine.app.Notification.Notification;
import com.astrosine.app.R;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HelpFragment extends Fragment {
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final Resources.Theme theme = getActivity().getTheme();
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ImageView backbutton = toolbar.findViewById(R.id.navigation);
        final AVLoadingIndicatorView progressBar = toolbar.findViewById(R.id.indicator);
        final ImageView notification = toolbar.findViewById(R.id.notification);
        notification.setVisibility(View.GONE);
        final FragmentManager  framentManager = getActivity().getSupportFragmentManager();
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Notification notification = new Notification();
                framentManager.beginTransaction().replace(R.id.content_main, notification).addToBackStack("Notification").commit();
            }
        });
        backbutton.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        TextView title = toolbar.findViewById(R.id.title);
        title.setText("QUERIES ON THE GO");
        final ImageView headerImage = view.findViewById(R.id.iv_main);
        final TextView tvTitle = view.findViewById(R.id.tv_title);
        final ExpandableListView expandableListView = view.findViewById(R.id.ex_lisview);
        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long id) {
                String selected = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition);
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra("question", selected);
                startActivity(intent);
                return true;

            }
        });
        NetworkResponse networkResponse = new NetworkResponse() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject body = jsonObject.getJSONObject("body");
                    String imageUrl = body.getString("image");
                    JSONObject data = body.getJSONObject("data");
                    String info = data.getString("info");
                    JSONArray jsonArray = data.getJSONArray("question");
                    tvTitle.setText(Html.fromHtml(info));
//                    Picasso.get().load(imageUrl).into(headerImage);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject header = jsonArray.getJSONObject(i);
                        String title = header.getString("title");
                        title = title + " ( Tap on Query )";
                        JSONArray infoData = header.getJSONArray("info");
                        listDataHeader.add(title);
                        List<String> childData = new ArrayList<String>();
                        for (int j = 0; j < infoData.length(); j++) {
                            JSONObject questionsObject = infoData.getJSONObject(j);
                            String questions = questionsObject.getString("question");
                            childData.add(questions);
                        }
                        listDataChild.put(title, childData);
                    }
                    ExpandableAdapter expandableAdapter = new ExpandableAdapter(getContext(), listDataHeader, listDataChild);
                    expandableListView.setAdapter(expandableAdapter);
                    progressBar.setVisibility(View.GONE);
                    notification.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String response) {

            }
        };
        Networking.getRequest(networkResponse, getContext(), Constants.HELP);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_help, container, false);
    }
}
