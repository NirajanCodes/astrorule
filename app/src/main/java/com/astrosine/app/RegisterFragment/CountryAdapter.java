package com.astrosine.app.RegisterFragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.astrosine.app.R;
import com.astrosine.app.Utility.Utility;

import java.util.ArrayList;

public class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.CountryViewHolder> implements Filterable {
    ArrayList<String> countries;
    ArrayList<String> filteredList;
    boolean nonFilerted = false;
    OnClick onClick;

    public CountryAdapter(ArrayList<String> countries, OnClick onClick) {
        this.countries = countries;
        this.onClick = onClick;
        this.filteredList = countries;
    }

    @NonNull
    @Override
    public CountryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new CountryViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyler_country, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final CountryViewHolder countryViewHolder, final int i) {
        countryViewHolder.countryName.setText(filteredList.get(i));
        countryViewHolder.countryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClick.onClick(filteredList.get(i));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (nonFilerted) {
            return filteredList.size();
        } else {
            return countries.size();
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                ArrayList<String> filteredListNew = new ArrayList<>();
                String name = charSequence.toString();
                if (name.isEmpty()) {
                    filteredListNew = countries;
                    nonFilerted = false;
                } else {
                    for (String list : filteredList) {
                        if (list.toUpperCase().contains(name.toUpperCase())) {
                            filteredListNew.add(list);
                            nonFilerted = true;
                        }
                    }
                }
                filteredList = filteredListNew;
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredList = (ArrayList<String>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class CountryViewHolder extends RecyclerView.ViewHolder {
        TextView countryName;

        public CountryViewHolder(@NonNull View itemView) {
            super(itemView);
            countryName = itemView.findViewById(R.id.textView);
        }
    }

    interface OnClick {
        void onClick(String countries);
    }
}
