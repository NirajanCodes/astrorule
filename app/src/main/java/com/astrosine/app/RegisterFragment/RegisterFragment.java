package com.astrosine.app.RegisterFragment;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.astrosine.app.Constants.Constants;
import com.astrosine.app.MainActivity;
import com.astrosine.app.Networking.NetworkResponse;
import com.astrosine.app.Networking.Networking;
import com.astrosine.app.R;
import com.astrosine.app.Utility.SharedPreferencesManager;
import com.astrosine.app.Utility.Utility;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class RegisterFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private static final int PERMISSION_REQUEST_CODE_GALLERY = 300;
    private EditText etName, etAddress, etEmail, etGnder, etDob, etTime, etAccuracy, etCountry, etStates, etCity;
    private RelativeLayout dateOfBirth, layoutCountry, layoutStates, rlProgressBar;
    private LinearLayout userImage, datePicker, timePicker;
    private TextInputLayout city;
    private TextView tvDob, tvTime, gallery, camara, doneTime, doneDate, cancelTime, cancelDate;
    private String[] countryNames, months, amPmList;
    private String[] statesNames;
    private RadioGroup gender;
    private String[] genderNames;
    private TextView title;
    private LinearLayout linearLayout;
    private Dialog dialog;
    private RadioButton[] radioButtons;
    private RadioGroup radioGroup;
    private int id;
    private int hour, minute, day, month, year = -1;
    private RadioButton timeAccuracy;
    private Button submit, done;
    private ArrayList<String> countries = null;
    private ArrayList<String> states = null;
    private int GALLERY = 1, CAMERA = 2;
    private ImageView ivUserImage;
    private static final String IMAGE_DIRECTORY = "/astrosine";
    private static final String IMAGE_NAME = "/userImage.png";
    private BottomSheetBehavior bottomSheetBehavior;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private File imageFile;
    private int accuracy;
    private String path = "/data/user/0/com.astrorule.app/app_imageDir";
    private String api = null;
    private Bitmap bitmap;
    private NumberPicker number_hour, number_minute, number_month, number_day, number_year, amPm;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        countries = new ArrayList<>();
        states = new ArrayList<>();
        gender = view.findViewById(R.id.gender);
        rlProgressBar = view.findViewById(R.id.progressBar);
        getCountryList();
        bottomSheetBehavior = BottomSheetBehavior.from(view.findViewById(R.id.bottom_nav));
        bottomSheetBehavior.setPeekHeight(10000);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        gallery = view.findViewById(R.id.gallery);
        camara = view.findViewById(R.id.camara);
        gallery.setOnClickListener(this);
        camara.setOnClickListener(this);
        genderNames = new String[]{"Male", "Female"};
        dialog = new Dialog((getContext()));
        doneDate = view.findViewById(R.id.done_date);
        doneTime = view.findViewById(R.id.done_time);
        cancelDate = view.findViewById(R.id.cancel_date);
        cancelTime = view.findViewById(R.id.cancel_time);
        cancelDate.setOnClickListener(this);
        cancelTime.setOnClickListener(this);
        doneDate.setOnClickListener(this);
        doneTime.setOnClickListener(this);
        timePicker = view.findViewById(R.id.time_picker);
        number_hour = view.findViewById(R.id.hour);
        number_minute = view.findViewById(R.id.minute);
        amPm = view.findViewById(R.id.amPm);
        amPmList = new String[]{"AM", "PM"};
        amPm.setDisplayedValues(amPmList);
        amPm.setMaxValue(1);
        number_month = view.findViewById(R.id.month);
        number_day = view.findViewById(R.id.day);
        number_year = view.findViewById(R.id.year);
        number_hour.setMaxValue(12);
        number_hour.setMinValue(1);
        number_minute.setMaxValue(59);
        number_day.setMaxValue(32);
        number_day.setMinValue(1);
        number_month.setMaxValue(11);
        months = new String[]{"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        number_month.setDisplayedValues(months);
        number_year.setMinValue(1920);
        number_year.setMaxValue(2019);
        etName = view.findViewById(R.id.et_name);
        etName.setSingleLine();
        etEmail = view.findViewById(R.id.et_email);
        etEmail.setSingleLine();
        etDob = view.findViewById(R.id.et_dob);
        etStates = view.findViewById(R.id.et_states);
        etStates.setOnClickListener(this);
        etCountry = view.findViewById(R.id.tv_country);
        etCountry.setOnClickListener(this);
        etDob.setOnClickListener(this);
        etTime = view.findViewById(R.id.et_tob);
        etTime.setOnClickListener(this);
        dialog.setContentView(R.layout.dialog_register);
        title = dialog.findViewById(R.id.title);
        timeAccuracy = view.findViewById(R.id.time_accuracy);
        userImage = view.findViewById(R.id.user_image);
        ivUserImage = view.findViewById(R.id.iv_user_image);
        userImage.setOnClickListener(this);
        datePicker = view.findViewById(R.id.date_picker);
        etCity = view.findViewById(R.id.et_city);
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        linearLayout = dialog.findViewById(R.id.main);
        submit = view.findViewById(R.id.submit);
        submit.setOnClickListener(this);
        final Resources.Theme theme = getActivity().getTheme();
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ImageView backbutton = toolbar.findViewById(R.id.navigation);
        backbutton.setImageDrawable(getResources().getDrawable(R.drawable.ic_back));
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().onBackPressed();
            }
        });
        Utility.hideKeyboard(getActivity());
        TextView title = toolbar.findViewById(R.id.title);
        title.setText("MY ACCOUNT ");
        final AVLoadingIndicatorView progressBar = toolbar.findViewById(R.id.indicator);
        progressBar.setVisibility(View.GONE);
        setData();
        try {
            Picasso.get().load(SharedPreferencesManager.getImageUrl()).placeholder(R.drawable.userprofile).into(ivUserImage);
        } catch (NullPointerException e) {

        }

        toolbar.findViewById(R.id.notification).setVisibility(View.GONE);
//        final ImageView notification = toolbar.findViewById(R.id.notification);
//        final FragmentManager  framentManager = getActivity().getSupportFragmentManager();
//        notification.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Notification notification = new Notification();
//                framentManager.beginTransaction().replace(R.id.content_main, notification).addToBackStack("Notification").commit();
//            }
//        });
    }

    public void setData() {
        if (SharedPreferencesManager.getRegistered()) {
            try {
                JSONObject jsonObject = new JSONObject(SharedPreferencesManager.getUserData());
                //  Bitmap bitmap= (Bitmap) jsonObject.get("image");
                //ivUserImage.setImageBitmap(bitmap);
                getFile();
                accuracy = jsonObject.getInt("time_accuracy");
                if (accuracy == 1) {
                    timeAccuracy.setChecked(true);
                } else {
                    timeAccuracy.setChecked(false);
                }
                String gender = jsonObject.getString("gender");
                if (gender.equalsIgnoreCase("m")) {
                    gender = "male";
                } else if (gender.equalsIgnoreCase("f")) {
                    gender = "female";
                }
                etEmail.setText(jsonObject.getString("email"));
                String[] location = jsonObject.getString("location").split(",");
                if (location.length > 1) {
                } else {
                    if (!jsonObject.getString("states").isEmpty() && jsonObject.getString("states").length() > 2) {
                        etStates.setText(jsonObject.getString("states"));
                    }
                    etCity.setText(jsonObject.getString("location"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.et_dob:
                datePicker.setVisibility(View.VISIBLE);
                break;
            case R.id.et_tob:
                timePicker.setVisibility(View.VISIBLE);
                break;
            case R.id.tv_country:
                etCountry.setText("  ");
                createDialogFragment();
                break;
            case R.id.et_states:
                if (states != null && states.size() > 2) {
                    dialog.show();
                    etStates.setText("  ");
                    createRadioButtons("STATES", statesNames, etStates, 500);
                }
                break;
            case R.id.done_date:
                datePicker.setVisibility(View.GONE);
                etDob.setText(number_year.getValue() + "-" + number_month.getValue() + "-" + number_day.getValue());
                break;
            case R.id.done_time:
                timePicker.setVisibility(View.GONE);
                etTime.setText(number_hour.getValue() + ":" + number_minute.getValue() + "  " + amPmList[amPm.getValue()]);
                break;
            case R.id.submit:
                validate();
                break;
            case R.id.user_image:
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;
            case R.id.gallery:
                choosePhotoFromGallary();
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                break;
            case R.id.camara:
                takePhotoFromCamera();
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                break;
            case R.id.cancel_date:
                datePicker.setVisibility(View.GONE);
                break;
            case R.id.cancel_time:
                timePicker.setVisibility(View.GONE);
                break;
        }
    }

    private void createDialogFragment() {
        final Dialog dialogSearch = new Dialog(getActivity(), WindowManager.LayoutParams.MATCH_PARENT);
        dialogSearch.setContentView(R.layout.dialog_fragment);
        SearchView searchView = dialogSearch.findViewById(R.id.search);
        searchView.setIconified(false);
        searchView.setIconifiedByDefault(true);
        searchView.setFocusable(true);
        CountryAdapter.OnClick onClick = new CountryAdapter.OnClick() {
            @Override
            public void onClick(String countries) {
                etCountry.setText(countries);
                dialogSearch.dismiss();
                getStateList();
            }
        };
        final CountryAdapter countryAdapter = new CountryAdapter(countries, onClick);
        RecyclerView dialogRecyler = dialogSearch.findViewById(R.id.dialog_recyler);
        dialogRecyler.setAdapter(countryAdapter);
        dialogRecyler.setLayoutManager(new LinearLayoutManager(getContext()));
        dialogSearch.show();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                countryAdapter.getFilter().filter(s);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                countryAdapter.getFilter().filter(s);
                return false;
            }
        });
    }

    public void getFile() {
        try {
            Picasso.get().load(SharedPreferencesManager.getImageUrl()).placeholder(R.drawable.placeholder).into(ivUserImage);
            File f = new File(path, "profile.png");
            //  imageFile = BitmapFactory.decodeFile(String.valueOf(f));
            Bitmap b = BitmapFactory.decodeStream(new FileInputStream(f));
            imageFile = f;
            ivUserImage.setImageBitmap(b);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            Picasso.get().load(SharedPreferencesManager.getImageUrl()).placeholder(R.drawable.placeholder).into(ivUserImage);
        } catch (Exception e) {

        }
    }

    public void createRadioButtons(String titleText, final String[] list,
                                   final TextView textView, int size) {
        try {
            linearLayout.removeAllViews();
            title.setText(titleText);
            title.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
            title.setTextColor(getResources().getColor(R.color.colorBlack));
            title.setTextSize(20f);
            radioButtons = new RadioButton[list.length];
            radioGroup = new RadioGroup(getContext());
            radioGroup.setOrientation(RadioGroup.VERTICAL);
            ScrollView scrollView = new ScrollView(getContext());
            scrollView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, size));
            scrollView.setPadding(50, 0, 10, 0);
            scrollView.addView(radioGroup);

            for (int i = 0; i < list.length; i++) {
                radioButtons[i] = new RadioButton(getContext());
                radioButtons[i].setId(i);
                radioButtons[i].setPadding(20, 30, 20, 30);
                radioGroup.addView(radioButtons[i]);
                radioButtons[i].setText(list[i]);
            }

            View view1 = new View(getContext());
            view1.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2));
            View view2 = new View(getContext());
            view2.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2));
            view1.setBackgroundColor(getResources().getColor(R.color.colorView));
            view2.setBackgroundColor(getResources().getColor(R.color.colorView));
            Button ok = new Button(getContext());
            ok.setText("ok");
            Button cancel = new Button(getContext());
            cancel.setText("cancel");
            cancel.setOnClickListener(this);

            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    id = radioGroup.getCheckedRadioButtonId();
                }
            });
            ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    textView.setText(radioButtons[id].getText());
                    dialog.dismiss();
                }
            });
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            cancel.setTextColor(getResources().getColor(R.color.colorAccent));
            ok.setTextColor(getResources().getColor(R.color.colorAccent));
            ok.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            cancel.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            LinearLayout linearLayoutButtons = new LinearLayout(getContext());
            linearLayoutButtons.setGravity(Gravity.RIGHT);
            linearLayoutButtons.setOrientation(LinearLayout.HORIZONTAL);
            linearLayoutButtons.addView(cancel);
            linearLayoutButtons.addView(ok);
            linearLayout.addView(title);
            linearLayout.addView(view1);
            linearLayout.addView(scrollView);
            linearLayout.addView(view2);
            linearLayout.addView(linearLayoutButtons);
        } catch (Exception e) {

        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        year = i;
        month = i1;
        day = i2;
        tvDob.setText(i1 + "|" + i2 + "|" + i);
    }

   /* @Override
    public void onTimeSet(TimePicker timePicker, int i, int i1) {
        if (i < 12) {
            hour = i;
            amPm = "AM";
        } else {
            hour = i - 12;
            amPm = "PM";
        }
        minute = i1;
        tvTime.setText(hour + ":" + i1 + " " + amPm);
    }*/

    public void choosePhotoFromGallary() {
        if (checkPermissionGallery()) {
            Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            galleryIntent.setType("image/*");
            startActivityForResult(galleryIntent, GALLERY);
        } else {
            requestPermissionGallery();
        }
    }

    private void takePhotoFromCamera() {
        if (checkPermission()) {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);
        } else {
            requestPermission();
        }
    }

    public void validate() {
        if (SharedPreferencesManager.getRegistered()) {
            api = Constants.PROFILE + "/" + SharedPreferencesManager.getUserId();
        } else {
            api = Constants.PROFILE;
        }
        String name = etName.getText().toString();
        String email = etEmail.getText().toString().trim();
        String country = etCountry.getText().toString().trim();
        int id = gender.getCheckedRadioButtonId();
        String genderString = null;
        RadioButton genderValue = getView().findViewById(id);
        if (genderValue != null) {
            genderString = genderValue.getText().toString();
        }
        String time = etTime.getText().toString().trim();
        String date = etDob.getText().toString().trim();

        accuracy = 0;
        final JSONObject jsonObject = new JSONObject();
        if (timeAccuracy.isChecked()) {
            accuracy = 1;
        }
        if (etCity == null && etCity.getText().length() < 2) {
            Toast.makeText(getContext(), "No fields should be empty", Toast.LENGTH_SHORT).show();

        } else if (!(name != null && country != null && etCity != null && genderString != null && etStates != null && !time.isEmpty() && !date.isEmpty())) {
            Toast.makeText(getContext(), "No fields should be empty", Toast.LENGTH_SHORT).show();
        }
        /* else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(getContext(), "Invalid Email", Toast.LENGTH_SHORT).show();

        }*/
        else {
            rlProgressBar.setVisibility(View.VISIBLE);
            NetworkResponse networkResponse = new NetworkResponse() {

                @Override
                public void onSuccess(String response) {
                    Log.d(Utility.TAG, "register " + response);
                    try {
                        JSONObject responseJson = new JSONObject(response);
                        JSONObject status = responseJson.getJSONObject("status");
                        JSONObject body = responseJson.getJSONObject("body");
                        int responseCode = status.getInt("responseCode");
                        if (responseCode == 200) {
                            path = saveImage(bitmap);
                            ivUserImage.setImageBitmap(bitmap);
                            SharedPreferencesManager.setRegistered(true);
                            SharedPreferencesManager.setUserData(jsonObject.toString());
                            SharedPreferencesManager.setUserId(body.optString("user_id"));

                            Toast.makeText(getContext(), "Registered Successfully", Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();

                        } else {
                            Toast.makeText(getContext(), "Validation Failed", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    rlProgressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError(String response) {
                    Log.d(Utility.TAG, "response :  " + response);

                    Toast.makeText(getContext(), "Registration Failed", Toast.LENGTH_SHORT).show();
                    rlProgressBar.setVisibility(View.GONE);
                }
            };
            try {
                String city;

                if (etStates.getText().toString().length() < 2) {
                    city = etCity.getText().toString();
                } else {
                    city = etStates.getText().toString() + "," + etCity.getText().toString();
                }
                if (SharedPreferencesManager.getRegistered()) {
                    jsonObject.put("date_of_birth", etDob.getText().toString());
                    SharedPreferencesManager.setUserDob(etDob.getText().toString());
                    jsonObject.put("time_of_birth", etTime.getText().toString());
                } else {
                    jsonObject.put("date_of_birth", number_year.getValue() + "-" + number_month.getValue() + "-" + number_day.getValue());
                    SharedPreferencesManager.setUserDob(number_year.getValue() + "-" + number_month.getValue() + "-" + number_day.getValue());
                    jsonObject.put("time_of_birth", number_hour.getValue() + ":" + number_minute.getValue() + " " + amPm);
                }
                jsonObject.put("name", name);
                SharedPreferencesManager.setUserName(name);
                jsonObject.put("country", country);
                jsonObject.put("image", imageFile);
                jsonObject.put("device_token", SharedPreferencesManager.getDeviceId());
                jsonObject.put("time_accuracy", accuracy);
                jsonObject.put("gender", genderString);
                jsonObject.put("email", email);
                jsonObject.put("device_type", "android");
                if (etStates.getText().toString().length() < 2) {
                    jsonObject.put("location", etCity.getText().toString());
                } else {
                    jsonObject.put("location", etStates.getText().toString() + "," + etCity.getText().toString());
                }
                jsonObject.put("gcm_id", "eIjJVKKaZro:APA91bELPT0VcMMopozaXNwgBmmB1jukTXJv8vWjIijvDCQLo9iQqfs60H5hIHVVWxmpAOH1KR2RkafWy_o-ElDkVY8yTxlI2KJHLuAdZ_U9LuDpB4Fl_EXo2sA6ZFl9AyO3Ettbc0ND");
                Log.d(Utility.TAG, "request :" + jsonObject.toString());
                //new AddOrderStatus(name, country, String.valueOf(accuracy), gender, email, city, date, time).execute();
                Networking.postMultipartRequest(networkResponse, jsonObject.toString(), api, imageFile);
            } catch (Exception e) {
                e.printStackTrace();
                rlProgressBar.setVisibility(View.GONE);
                Toast.makeText(getContext(), "Registered Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), contentURI);
                    Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
                    saveImage(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        } else if (requestCode == CAMERA) {
            bitmap = (Bitmap) data.getExtras().get("data");
            saveImage(bitmap);
            Toast.makeText(getContext(), "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    public String saveImage(Bitmap myBitmap) {
        ivUserImage.setImageBitmap(myBitmap);
       /* if(imageFile!=null){

            req = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("medicine", name.getText().toString())
                    .addFormDataPart("token", mGlobalVariable.getUserData(DataHolder.TOKEN))
                    .addFormDataPart("quantity", quantity.getText().toString())
                    .addFormDataPart("photo", "order.png", RequestBody.create(MEDIA_TYPE_PNG, imageFile)).build();

        }else{

            req = new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("medicine", name.getText().toString())
                    .addFormDataPart("token", mGlobalVariable.getUserData(DataHolder.TOKEN))
                    .addFormDataPart("quantity", quantity.getText().toString()).build();

        }*/

        ContextWrapper cw = new ContextWrapper(getContext());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, "profile.png");
        //  imageFile = BitmapFactory.decodeFile(directory + "profile.jpg");
        imageFile = mypath;
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);

            // Use the compress method on the BitMap object to write image to the OutputStream
            myBitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.d(Utility.TAG, " nirajan " + directory.getAbsolutePath());
        return directory.getAbsolutePath();
    }

    public void getCountryList() {
        NetworkResponse networkResponse = new NetworkResponse() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray body = jsonObject.getJSONArray("body");
                    for (int i = 0; i < body.length(); i++) {
                        JSONArray data = body.getJSONArray(i);
                        countries.add(data.toString().replace("[", "").replace("]", "")
                                .replace("[[", "").replace("]]", "").replace("\"", "")
                        );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                countryNames = countries.toString().replace("[", "").replace("]", "")
                        .replace("[[", "").replace("]]", "").replace("\"", "")
                        .split(",");
                Log.d(Utility.TAG, " countries " + countryNames.toString());
            }

            @Override
            public void onError(String response) {

            }
        };
        Networking.getRequest(networkResponse, getContext(), Constants.COUNTRY);
    }

    public void getStateList() {
        etStates.setOnClickListener(this);
        etStates.setText("");
        NetworkResponse networkResponse = new NetworkResponse() {
            @Override
            public void onSuccess(String response) {
                rlProgressBar.setVisibility(View.GONE);
                statesNames = null;
                states.clear();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray body = jsonObject.getJSONArray("body");
                    JSONObject status = jsonObject.getJSONObject("status");
                    int resCode = status.getInt("responseCode");
                    if (resCode == 200) {
                        for (int i = 0; i < body.length(); i++) {
                            JSONObject data = body.getJSONObject(i);
                            String statesName = data.getString("name");
                            states.add(statesName);
                        }
                        if (states.size() < 2) {
                            etStates.setOnClickListener(null);
                        } else {
                            etStates.setFocusable(true);

                        }
                    } else {
                        Toast.makeText(getContext(), "Response Error", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                statesNames = states.toString().replace("[", "").replace("]", "").split(",");
            }

            @Override
            public void onError(String response) {

            }
        };
        Log.d(Utility.TAG, " countries " + etCountry.getText().toString());
        rlProgressBar.setVisibility(View.VISIBLE);
        Networking.getRequest(networkResponse, getContext(), "https://astrorule.com/apis/public/api/" + etCountry.getText().toString().trim() + "/states");
        Log.d(Utility.TAG, " url :  https://astrorule.com/apis/public/api/" + etCountry.getText().toString().trim() + "/states");
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private boolean checkPermissionGallery() {
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {
        requestPermissions(
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }

    private void requestPermissionGallery() {
        requestPermissions(
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                PERMISSION_REQUEST_CODE_GALLERY);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission Granted", Toast.LENGTH_SHORT).show();
                    takePhotoFromCamera();

                } else {
                    Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                        }
                    }
                }
                break;
            case PERMISSION_REQUEST_CODE_GALLERY:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getActivity(), "Permission Granted", Toast.LENGTH_SHORT).show();
                    choosePhotoFromGallary();
                } else {
                    Toast.makeText(getContext(), "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                        }
                    }
                }
        }
    }
}
/*

    class AddOrderStatus extends AsyncTask<String, String, String> {
        String name = null, country = null, accuracy = null, gender = null, email = null, city = null, dob = null, tob = null;

        public AddOrderStatus(String name, String country, String accuracy, String gender, String email, String city, String dob, String tob) {
            this.name = name;
            this.accuracy = accuracy;
            this.country = country;
            this.gender = gender;
            this.email = email;
            this.city = city;
            this.tob = tob;
            this.dob = dob;

        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            String result = null;
            MultipartBody req;
            if (city.length() < 2) {
                city = "kathmandu";
            }
            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
            if (imageFile == null) {
                req = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("name", name)
                        .addFormDataPart("country", country)
                        .addFormDataPart("device_token", SharedPreferencesManager.getDeviceId())
                        .addFormDataPart("time_accuracy", String.valueOf(accuracy))
                        .addFormDataPart("gender", gender)
                        .addFormDataPart("email", email)
                        .addFormDataPart("device_type", "android")
                        .addFormDataPart("location", city)
                        .addFormDataPart("gcm_id", SharedPreferencesManager.getGcmId())
                        .addFormDataPart("date_of_birth", dob)
                        .addFormDataPart("time_of_birth", tob).build();
            } else {
                req = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("name", name)
                        .addFormDataPart("country", country)
                        .addFormDataPart("image", "profile", RequestBody.create(MEDIA_TYPE_PNG, imageFile))
                        .addFormDataPart("device_token", SharedPreferencesManager.getDeviceId())
                        .addFormDataPart("time_accuracy", String.valueOf(accuracy))
                        .addFormDataPart("gender", gender)
                        .addFormDataPart("email", email)
                        .addFormDataPart("device_type", "android")
                        .addFormDataPart("location", city)
                        .addFormDataPart("gcm_id", SharedPreferencesManager.getGcmId())
                        .addFormDataPart("date_of_birth", dob)
                        .addFormDataPart("time_of_birth", tob).build();
            }

            try {
                OkHttpClient client = new OkHttpClient.Builder()
                        .connectTimeout(45, TimeUnit.SECONDS)
                        .writeTimeout(45, TimeUnit.SECONDS)
                        .readTimeout(45, TimeUnit.SECONDS)
                        .build();

                Request request = new Request.Builder()
                        .url(api)
                        .post(req)
                        .build();
                Response response = client.newCall(request).execute();
                result = response.body().string();
                return response.body().string();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d(Utility.TAG, " api  " + api);
            Log.d(Utility.TAG, " response  " + s);
            rlProgressBar.setVisibility(View.GONE);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONObject status = jsonObject.getJSONObject("status");
                int resCode = status.getInt("responseCode");
                if (resCode == 200) {
                    Toast.makeText(getContext(), "Registed Sucessfully", Toast.LENGTH_SHORT).show();
                    getActivity().onBackPressed();
                } else {
                    Toast.makeText(getContext(), "Registed Failed", Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(s);
                if (jsonObject.getString("success").equals("true")) {
                    String message = jsonObject.optString("message");
                } else {
                    if (jsonObject.getString("error").equals("TOKEN_EXPIRED")) {
                    } else {
                        String message = jsonObject.optString("error");
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

}
*/
