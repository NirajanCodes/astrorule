package com.astrosine.app.WelcomeActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astrosine.app.MainActivity;
import com.astrosine.app.R;
import com.astrosine.app.Utility.SharedPreferencesManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;


public class WelcomeActivity extends AppCompatActivity {
    private int[] layouts;
    private LinearLayout dotsLayout;
    private Button start, next;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        setTheme(R.style.AppTheme);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                try{
                    if (task.getResult() != null) {
                        String token = task.getResult().getToken();
                        Log.d("nirajan", token);
                        SharedPreferencesManager.setGcmId(token);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        if (SharedPreferencesManager.getFirstTime()) {
            Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            String android_id = Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
            SharedPreferencesManager.setDeviceId(android_id);
            setTheme(R.style.AppTheme);
            dotsLayout = findViewById(R.id.layoutDots);
            start = findViewById(R.id.start);
            // next = findViewById(R.id.next);
            /*start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferencesManager.setFirstTime(true);
                Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });*/
            layouts = new int[]{
                    R.layout.view_pager_first,
                    R.layout.view_pager_second,
                    R.layout.view_pager_third,
                    R.layout.view_pager_forth
            };

//            MyPagerAdapters myPagerAdapters = new MyPagerAdapters(this, layouts);
            WelcomeAdapter myPagerAdapters = new WelcomeAdapter(getSupportFragmentManager());
            ViewPager viewPager = findViewById(R.id.view_pager);
            viewPager.setAdapter(myPagerAdapters);
            viewPager.addOnPageChangeListener(pageChangeListener);
            addBottomDots(0);
        }
    }

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {

            if (i == layouts.length - 1) {
                Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }

        @Override
        public void onPageSelected(int i) {
            addBottomDots(i);
        }

        @Override
        public void onPageScrollStateChanged(int i) {


        }
    };

    public void addBottomDots(int currentPage) {
        TextView[] dots = new TextView[layouts.length];
        int colorActive = getResources().getColor(R.color.colorPrimary);
        int colorInactive = getResources().getColor(R.color.colorAccent);
        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length - 1; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorInactive);
            dotsLayout.addView(dots[i]);
        }
        try {
            if (dots.length  > 0) {
                dots[currentPage].setTextColor(colorActive);
            }
        } catch (Exception e) {

        }
    }
}
